package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gopkg.in/yaml.v2"

	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"reflect"
	"strings"
	"time"
)

var (
	fileAccessDelays = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "file_access_delay_seconds",
			Help: "Time to get file metainfo.",
		},
		[]string{"filename", "cmd"},
	)

	listenInterface = ""
)

func init() {
	prometheus.MustRegister(fileAccessDelays)
}

type Config struct {
	Filenames     []string      `yaml:"filenames"`
	Cmds          []string      `yaml:"cmds"`
	Port          string        `yaml:"port"`
	CheckInterval time.Duration `yaml:"check_interval"`
}

func getConfig() (Config, error) {
	// 1. Get cmdline
	confFile := flag.String("config", "", "Configuration file")
	tgtFiles := flag.String("filename", "", "Comma-separated list of files to check")
	commands := flag.String("commands", "", "Comma-separated list of commands to run")
	interval := flag.String("interval", "", "Time interval between subsequent checks")
	port := flag.String("port", "", "Port to listen on")

	flag.Parse()

	// 2. Try set default config values and read the confFile
	config := Config{
		Port:          "9930",
		CheckInterval: 10 * time.Second,
	}

	if *confFile != "" {
		confData, err := ioutil.ReadFile(*confFile)
		if err != nil {
			return Config{}, fmt.Errorf("Failed to read '%s': %w", *confFile, err)
		}

		err = yaml.Unmarshal(confData, &config)
		if err != nil {
			return Config{}, fmt.Errorf("Failed to parse '%s': %w", *confFile, err)
		}
	}

	// TODO: overwrite or append?
	// 3. Overwrite the config values with cmdline
	if *tgtFiles != "" {
		config.Filenames = strings.Split(*tgtFiles, ",")
	}
	if *commands != "" {
		config.Cmds = strings.Split(*commands, ",")
	}
	if *interval != "" {
		duration, err := time.ParseDuration(*interval)
		if err != nil {
			return Config{}, fmt.Errorf("Invalid interval flag: %w", err)
		}
		config.CheckInterval = duration
	}
	if *port != "" {
		config.Port = *port
	}

	return config, nil
}

func trackTime(start time.Time, labels prometheus.Labels) {
	elapsed := time.Since(start)
	fileAccessDelays.With(labels).Set(elapsed.Seconds())
}

type Task struct {
	CmdString string
	Filename  string
	Ready     chan int
	Cmd       *exec.Cmd
}

func (t *Task) Run() error {
	cmdSplit := append(strings.Fields(t.CmdString), t.Filename)

	defer trackTime(
		time.Now(),
		prometheus.Labels{
			"filename": t.Filename,
			"cmd":      t.CmdString,
		},
	)
	t.Cmd = exec.Command(cmdSplit[0], cmdSplit[1:]...)
	return t.Cmd.Run()
}

// We should not spawn goroutines every check_interval --- we can run out
// of goroutines in case some check is hanging forever. Thus there is
// always only one goroutine per file+cmd, that is restarted check_interval
// after the previous check is finished.
func runChecks(config Config) {
	checksReady := make([]reflect.SelectCase, 0)
	tasks := make([]Task, 0)

	for _, filename := range config.Filenames {
		for _, cmd := range config.Cmds {
			tasks = append(tasks, Task{
				CmdString: cmd,
				Filename:  filename,
				Ready:     make(chan int, 1),
			})
			// Set all chans to "ready"
			tasks[len(tasks)-1].Ready <- 1

			// List is built dynamically, so we cannot use simple select
			reflectedChan := reflect.SelectCase{
				Dir:  reflect.SelectRecv,
				Chan: reflect.ValueOf(tasks[len(tasks)-1].Ready),
			}
			checksReady = append(checksReady, reflectedChan)
		}
	}

	for {
		chosen, _, ok := reflect.Select(checksReady)
		if !ok {
			log.Printf("Task %+v is dead...", tasks[chosen])
			checksReady[chosen].Chan = reflect.ValueOf(nil)
		}

		// Run selected channel
		go func(t *Task) {
			log.Printf("Starting task: %+v", t)
			err := t.Run()
			if err != nil {
				log.Printf("Task %+v failed: %s", t, err.Error())
			}
			time.Sleep(config.CheckInterval)
			t.Ready <- 1
		}(&tasks[chosen])
	}
}

func main() {
	config, err := getConfig()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Current running config: %+v", config)

	go runChecks(config)

	http.Handle("/metrics", promhttp.Handler())
	err = http.ListenAndServe(listenInterface+":"+config.Port, nil)
	log.Fatalf("Failed to ListenAndServe: %s", err.Error())
}
