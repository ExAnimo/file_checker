
# FileChecker

This is a utility to export command execution time to Prometheus.

# Installation

Clone the repository and build the binary:
```bash
git clone https://gitlab.com/ExAnimo/file_checker.git
cd file_checker
go build
```

The statically-linked binary, called `file_checker`, will appear in the
current directory.

# Configuration

Configuration can be set in 2 ways:

1. Using the config file:  
Run `./file_checker -config=your_cfg.yaml`.  
Check out the `example.yaml` for details.

2. Via the command line arguments:  
Run `./file_checker -h` to get the list of arguments available.

NOTE: you can overwrite config with command line arguments.

Examples:

1. Just use config:
```bash
./file_checker -config=example.yaml
```
2. Just use cmdline args (analog of example.yaml):
```bash
./file_checker -filename='/dev/null,/home/' -commands='ls,ls -al,stat' -port 9931 -interval 2s
```
3. Use config, but set different port:
```bash
./file_checker -config=example.yaml -port 9931
```

# Testing

To test configuration and ensure that the utility works as expected,
run the following commands:
```bash
# 1. Run example configuration, redirect output to /dev/null, send the task to backgroud
./file_checker -config=example.yaml > /dev/null 2>&1 &
# 2. Query the metrics, grep the "interesting" ones
curl http://localhost:9931/metrics 2>/dev/null | grep file_access
# 3. Return the utility to the foregroud
fg
# 4. Stop
# Press Ctrl+C to stop the file_checker
```

`curl` should show 6 values for the cartesian product of cmds x filenames.

